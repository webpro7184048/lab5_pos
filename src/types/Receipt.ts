import type { Member } from "./Member";
import type { ReceiptItem } from "./ReceiptItem";
import type { User } from "./User";

type Receipt = {
    id: number;
    createdDate: Date;
    totalBefore: number;
    total: number;
    memberDiscount: number;
    receivedAmount: number;
    change: number;
    paymentType: string;
    userId: number;
    memberId: number;
    user?: User;
    member?: Member;
    receiptItems?: ReceiptItem[]
}

export type {Receipt}