import { ref, computed } from 'vue'
import { defineStore } from 'pinia'
import type { Member } from '@/types/Member'

export const useMemberStore = defineStore('member', () => {
  const members = ref<Member[]>([
    {id:1, name: 'mana rukchat', tel: '0123456789'},
    {id:2, name: 'manee meejai', tel: '0987654321'},
  ])
  const currentMember = ref<Member | null>()
  const searchMember = (tel: string)  => {
    const index = members.value.findIndex((item)=> item.tel ===tel)
    if(index<0) {
        currentMember.value = null
    }
    currentMember.value = members.value[index]
}
function clear(){
    currentMember.value = null
}
  return { members, searchMember, currentMember,clear }
})